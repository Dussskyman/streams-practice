import 'package:flutter/material.dart';
import 'package:stream_practice/slider_color.dart';
import 'package:stream_practice/stream_color.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamColor _streamColor = StreamColor();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: _streamColor.color,
        builder: (context, AsyncSnapshot<Color> snapshot) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ColorSlider(
              color: Colors.red,
              value: snapshot.data.red.toDouble(),
              onChanged: _streamColor.changeRed,
            ),
            ColorSlider(
              color: Colors.green,
              value: snapshot.data.green.toDouble(),
              onChanged: _streamColor.changeGreen,
            ),
            ColorSlider(
              color: Colors.blue,
              value: snapshot.data.blue.toDouble(),
              onChanged: _streamColor.changeBlue,
            ),
            ColorSlider(
              useForOpacity: true,
              color: snapshot.data,
              value: snapshot.data.opacity,
              onChangedDouble: _streamColor.changeOpacity,
            ),
            Container(
              width: 200,
              height: 200,
              color: snapshot.data,
            )
          ],
        ),
      ),
    );
  }
}
