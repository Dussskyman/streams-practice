import 'package:flutter/material.dart';

class ColorSlider extends StatelessWidget {
  final double value;
  final void Function(int i) onChanged;
  final void Function(double i) onChangedDouble;
  final bool useForOpacity;
  final Color color;

  const ColorSlider({
    Key key,
    this.value,
    this.onChanged,
    this.useForOpacity = false,
    this.color,
    this.onChangedDouble,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slider(
      activeColor: color,
      min: 0,
      max: useForOpacity ? 1 : 255,
      value: value,
      onChanged: (value) {
        if (useForOpacity) {
          onChangedDouble(value);
          return;
        }
        onChanged(value.round());
      },
    );
  }
}
