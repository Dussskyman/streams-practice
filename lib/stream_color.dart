import 'dart:async';

import 'package:flutter/material.dart';

class StreamColor {
  int _red = 0;
  int _green = 0;
  int _blue = 0;

  double _opacity = 1;

  StreamColor() {
    _addColorValToStream();
  }

  StreamController _streamControllerColor = StreamController<Color>();

  Stream<Color> get color => _streamControllerColor.stream;

  void changeRed(int val) {
    _red = val;
    _addColorValToStream();
  }

  void changeGreen(int val) {
    _green = val;
    _addColorValToStream();
  }

  void changeBlue(int val) {
    _blue = val;
    _addColorValToStream();
  }

  void changeOpacity(double val) {
    _opacity = val;
    _addColorValToStream();
  }

  void _addColorValToStream() =>
      _streamControllerColor.add(Color.fromRGBO(_red, _green, _blue, _opacity));
}
